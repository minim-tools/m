exports.categories =
{
	value: "value",
	component: "component",
	map: "map",
	unary_operator: "unary operator",
	binary_operator: "binary operator",
	system: "system",
	gpu_system: "gpu system",
	vertex_component: "vertex component",
	fragment_component: "fragment component",
	material_component: "material component",
	gpu_map: "shader function",
	gpu_value: "value"
}
exports.types =
{
	proposition: "proposition",
	number: "number",
	vector: "vector",
	quaternion: "quaternion",

	text: "text",
	mesh: "mesh",
	material: "material",
	image: "image",
	color: "color",
	audio_clip: "audio clip",
	animator_controller: "animator controller",

	entity: "entity",
	entity_list: "entity list",

	curve: "curve",
	input: "input",
	anything: "anything",
}

const { proposition, number, vector, quaternion, text, mesh, material, color, image, audio_clip, animator_controller, entity, entity_list, input, curve, anything } = exports.types;
const { value, component, map, unary_operator, binary_operator, system, gpu_system, vertex_component, fragment_component, material_component, gpu_map, gpu_value } = exports.categories;

exports.library =
{
	[value]:
	{
		true: { semantic_type: proposition, constant: true },
		false: { semantic_type: proposition, constant: true },

		zero: { semantic_type: number, constant: true },
		one: { semantic_type: number, constant: true },

		epsilon: { semantic_type: number, constant: true },
		pi: { semantic_type: number, constant: true },
		e: { semantic_type: number, constant: true },
		infinity: { semantic_type: number, constant: true },

		up: { semantic_type: vector, constant: true },
		right: { semantic_type: vector, constant: true },
		forward: { semantic_type: vector, constant: true },

		default_audio_input: { semantic_type: text, constant: true },
		usb_audio_input: { semantic_type: text, constant: true },
		hdmi_audio_input: { semantic_type: text, constant: true },

		last_frame_duration: { semantic_type: number, constant: true },
		process_duration: { semantic_type: number, constant: true },
		time_speed: { semantic_type: number, constant: false },

		screen_width: { semantic_type: number, constant: false },
		screen_height: { semantic_type: number, constant: false },
	},
	[component]:
	{
		albedo : { semantic_type: color },
		listening_ip: { semantic_type: text },
		listening_port: { semantic_type: number },
		connecting_ip: { semantic_type: text},
		connecting_port: { semantic_type: number },

		position: { semantic_type: vector },
		rotation: { semantic_type: quaternion },
		scale: { semantic_type: vector },
		world_position: { semantic_type: vector },
		world_rotation: { semantic_type: quaternion },

		kinematic: { semantic_type: proposition },
		mass: { semantic_type: number },
		angular_mass: { semantic_type: vector },
		velocity: { semantic_type: vector },
		angular_velocity: { semantic_type: vector },
		force: { semantic_type: vector },
		angular_force: { semantic_type: vector },

		box_center: { semantic_type: vector },
		extents: { semantic_type: vector },
		sphere_center: { semantic_type: vector },
		radius: { semantic_type: number },
		collisions: { semantic_type: entity_list },

		mesh: { semantic_type: mesh },
		animator: { semantic_type: entity },
		animator_controller: { semantic_type: animator_controller },
		shader: { semantic_type: entity },
		material: { semantic_type: material },

		near_plane: { semantic_type: number },
		far_plane: { semantic_type: number },
		field_of_view: { semantic_type: number },
		perspective: { semantic_type: proposition },
		display: { semantic_type: number },
		viewport_width: { semantic_type: number },
		viewport_height: { semantic_type: number },
		viewport_x: { semantic_type: number },
		viewport_y: { semantic_type: number },
		background: { semantic_type: color },

		emission: { semantic_type: color },
		cookie: { semantic_type: image },
		cookie_size: { semantic_type: number },
		intensity: { semantic_type: number },
		bounce_intensity: { semantic_type: number },
		spot_angle: { semantic_type: number },
		range: { semantic_type: number },

		audio_clip: { semantic_type: audio_clip },
		volume: { semantic_type: number },
		pitch: { semantic_type: number },
		loop: { semantic_type: proposition },
		mute: { semantic_type: proposition },

		destination: { semantic_type: vector },
		traversable_areas: { semantic_type: number },
		max_speed: { semantic_type: number },
		max_angular_speed: { semantic_type: number },
		max_acceleration: { semantic_type: number },

		deathmark: { semantic_type: proposition },
		presence: { semantic_type: proposition },
	},
	[map]:
	{
		accept_connection: { return_type: number, parameters: [{name: "listening port", type: number}], documentation: "Returns infinity if no client tried connecting this frame"},
		input_triggered: { return_type: proposition, parameters: [{name: "device", type: input}, {name: "client", type: number}], documentation: "Check whether the given client's input has been triggered"},
		input_number: { return_type: number, parameters: [{name: "device", type: input}, {name: "client", type: number}], documentation: "The numerical value provided by the given client for this input"},

		abs: { return_type: number, parameters: [{name: "x", type: number}], documentation: "Absolute value of the given number"},
		integer_part: { return_type: number, parameters: [{name: "x", type: number}], documentation: "The integer part of the given number"},
		sin: { return_type: number, parameters: [{name: "x", type: number}], documentation: "The sine of the given number"},
		cos: { return_type: number, parameters: [{name: "x", type: number}], documentation: "The cosine of the given number"},
		tan: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		asin: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		acos: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		atan: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		exp: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		log: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		sqrt: { return_type: number, parameters: [{name: "x", type: number}], documentation: ""},
		min: { return_type: number, parameters: [{name: "x", type: number}, {name: "y", type: number}], documentation: "The minimum value between the given numbers"},
		max: { return_type: number, parameters: [{name: "x", type: number}, {name: "y", type: number}], documentation: "The maximum value between the given numbers"},
		pow: { return_type: number, parameters: [{name: "base", type: number}, {name: "power", type: number}], documentation: "Elevate the given base to the given power"},
		random: { return_type: number, parameters: [{name: "min", type: number}, {name: "max", type: number}], documentation: "Random number between the given minimum and maximum values"},
		cross: { return_type: vector, parameters: [{name: "x", type: vector}, {name: "y", type: vector}], documentation: "Cross product of the given vectors"},
		dot: { return_type: number, parameters: [{name: "x", type: vector}, {name: "y", type: vector}], documentation: ""},
		length: { return_type: number, parameters: [{name: "x", type: vector}], documentation: ""},
		distance: { return_type: number, parameters: [{name: "x", type: vector}, {name: "y", type: vector}], documentation: ""},
		spherical_interpolation: { return_type: quaternion, parameters: [{name: "start", type: quaternion}, {name: "end", type: quaternion}, {name: "percentage", type: number}], documentation: ""},

		xyz: { return_type: vector, parameters: [{name: "x", type: number}, {name: "y", type: number}, {name: "z", type: number}], documentation: ""},
		x: { return_type: number, parameters: [{name: "vector", type: vector}], documentation: ""},
		y: { return_type: number, parameters: [{name: "vector", type: vector}], documentation: ""},
		z: { return_type: number, parameters: [{name: "vector", type: vector}], documentation: ""},
		quaternion: { return_type: quaternion, parameters: [{name: "vector", type: vector}], documentation: ""},
		vector: { return_type: vector, parameters: [{name: "quaternion", type: quaternion}], documentation: ""},
		viewport: { return_type: vector, parameters: [{name: "world coordinates", type: vector}, entity], documentation: ""},
		world: { return_type: vector, parameters: [{name: "viewport coordinates", type: vector}, entity], documentation: ""},
		text: { return_type: text, parameters: [{name: "number", type: number}], documentation: ""},
		number: { return_type: number, parameters: [{name: "text", type: text}], documentation: ""},
		degrees: { return_type: number, parameters: [{name: "radians", type: number}], documentation: ""},
		radians: { return_type: number, parameters: [{name: "degrees", type: number}], documentation: ""},

		rgba: { return_type: color, parameters: [{name: "red", type: number}, {name: "green", type: number}, {name: "blue", type: number}, {name: "alpha", type: number}], documentation: ""},
		red: { return_type: number, parameters: [{name: "color", type: color}], documentation: ""},
		green: { return_type: number, parameters: [{name: "color", type: color}], documentation: ""},
		blue: { return_type: number, parameters: [{name: "color", type: color}], documentation: ""},
		alpha: { return_type: number, parameters: [{name: "color", type: color}], documentation: ""},

		create: { return_type: entity, parameters: [{name: "entity", type: entity}], documentation: ""},
		evaluate: { return_type: number, parameters: [{name: "curve", type: curve}, {name: "abscissa", type: number}], documentation: ""},

		ray: { return_type: entity_list, parameters: [{name: "from", type: vector}, {name: "direction", type: vector}, {name: "distance", type: number}], documentation: ""},
		box: { return_type: entity_list, parameters: [{name: "origin", type: vector}, {name: "extents", type: vector}, {name: "orientation", type: quaternion}], documentation: ""},
		sphere: { return_type: entity_list, parameters: [{name: "origin", type: vector}, {name: "radius", type: number}], documentation: ""},

		save: { return_type: proposition, parameters: [{name: "filename", type:text}], documentation: "save game state to the given file"},
		load: { return_type: proposition, parameters: [{name: "filename", type:text}], documentation: "load game state from the given file"},
	},
	[unary_operator]:
	{
		"!": { return_type: proposition, parameters: [{name: "", type: proposition}]},
		"~": { return_type: number, parameters: [{name: "", type: number}]},
		"-": { return_type: number, parameters: [{name: "", type: number}]},
	},
	[binary_operator]:
	{
		"||": { return_type: proposition, parameters: [{name: "", type: proposition}, {name: "", type: proposition}]},
		"&&": { return_type: proposition, parameters: [{name: "", type: proposition}, {name: "", type: proposition}]},
		"|": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"&": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"^": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"==": { return_type: proposition, parameters: [{name: "", type: anything}, {name: "", type: anything}]},
		"!=": { return_type: proposition, parameters: [{name: "", type: anything}, {name: "", type: anything}]},
		"<": { return_type: proposition, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"<=": { return_type: proposition, parameters: [{name: "", type: number}, {name: "", type: number}]},
		">=": { return_type: proposition, parameters: [{name: "", type: number}, {name: "", type: number}]},
		">": { return_type: proposition, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"<<": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		">>": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"+": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"-": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"*": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"/": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"%": { return_type: number, parameters: [{name: "", type: number}, {name: "", type: number}]},
		"+++": { return_type: vector, parameters: [{name: "", type: vector}, {name: "", type: vector}]},
		"---": { return_type: vector, parameters: [{name: "", type: vector}, {name: "", type: vector}]},
		"***": { return_type: vector, parameters: [{name: "", type: vector}, {name: "", type: number}]},
		"///": { return_type: vector, parameters: [{name: "", type: vector}, {name: "", type: number}]},
	},
	[system]:
	{
		physics: {},
		pathfinding: {},
		rendering: {}
	},
	[gpu_system]:
	{

	},
	[vertex_component]:
	{
		position: { semantic_type: vector },
		normal: { semantic_type: vector },
		tangent: { semantic_type: vector },
		binormal: { semantic_type: vector },
		color: { semantic_type: color },
		texture_zero: { semantic_type: vector },
		texture_one: { semantic_type: vector },
		texture_two: { semantic_type: vector },
		texture_three: { semantic_type: vector },
	},
	[fragment_component]:
	{
		position: { semantic_type: vector, output: false },
		normal: { semantic_type: vector, output: false },
		tangent: { semantic_type: vector, output: false },
		binormal: { semantic_type: vector, output: false },
		color: { semantic_type: color, output: false },
		texture_zero: { semantic_type: vector, output: false },
		texture_one: { semantic_type: vector, output: false },
		texture_two: { semantic_type: vector, output: false },
		texture_three: { semantic_type: vector, output: false },

		depth: { semantic_type: number, output: true },
		stencil: { semantic_type: number, output: true },
		albedo: { semantic_type: color, output: true },
		metallic: { semantic_type: number, output: true },
		smoothness: { semantic_type: number, output: true },
		occlusion: { semantic_type: number, output: true },
	},
	[material_component]:
	{

	},
	[gpu_map]:
	{
		perlin: { return_type: image, parameters: [{name: "size", type: number}]},
		sample: { return_type: color, parameters: [{name: "texture", type: image}, {name: "point", type: vector}]},
		step: { return_type: number, parameters: [{name: "signal", type: number}, {name: "edge", type: number}]},
		rgb_a: { return_type: color, parameters: [{name: "rgb", type: vector}, {name: "alpha", type: number}]},
	},
	[gpu_value]:
	{
		true: { semantic_type: proposition, constant: true },
		false: { semantic_type: proposition, constant: true },

		zero: { semantic_type: number, constant: true },
		one: { semantic_type: number, constant: true },

		pi: { semantic_type: number, constant: true },
		e: { semantic_type: number, constant: true },

		up: { semantic_type: vector, constant: true },
		right: { semantic_type: vector, constant: true },
		forward: { semantic_type: vector, constant: true },

		last_frame_duration: { semantic_type: number, constant: true },
		process_duration: { semantic_type: number, constant: true },

		screen_width: { semantic_type: number, constant: false },
		screen_height: { semantic_type: number, constant: false },
	},
}
