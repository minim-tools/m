const { infer_implicit_information } = require("./inference");
const { system, shader, component, role, component_metadata, reflection } = require("./unity/artifacts");

function generate (tree, generator, text)
{
	infer_implicit_information(tree);

	generator.remove("Systems");
	generator.remove("Shaders");
	generator.remove("Components");
	generator.remove("Roles");

	for (const program of tree.programs || [])
	{
		if (program.syntactic_type === "system")
		{
			generator.create(system(program), `Systems/${program.name.text}.cs`);
			for (const [name, components] of Object.entries(program.roles))
			{
				generator.create(role(program.name.text, name, Array.from(components), tree.components), `Roles/${program.name.text}_${name}.cs`);
			}
		}
		else if (program.syntactic_type === "shader")
		{
			generator.create(shader(program), `Shaders/${program.name.text}.shader`);
		}
	}

	for (const [name, type] of Object.entries(tree.components) || [])
	{
		generator.create(component(name, type), `Components/${name}.cs`);
		generator.create(component_metadata(name), `Components/${name}.cs.meta`);
	}

	generator.create(reflection(tree.programs.filter(x => x.syntactic_type === "system"), text), `Editor/Reflection.cs`);
}

module.exports = { generate };
