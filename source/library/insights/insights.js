const { library, categories } = require("../../language/standard_library");

class Insights
{
	constructor ()
	{
		this.leaves = [];
		this.semantic_token_types = ["function", "variable", "property", "keyword", "operator", "type"];
		this.semantic_token_modifiers = ["defaultLibrary", "readonly"];
	}

	hover (position)
	{
		const node = (this.leaves[position.line] || []).find(x => x.position.start.character <= position.character && x.position.end.character >= position.character + 1);

		if (node === undefined) return "";

		const rules =
		{
			"identifier":
			[
				["system", "name", "system"],
				["iteration", "iterator", "value"],
				["functional", "name", "map"],
				["value", "value", "value"],
				["address", "root", "value"],
				["address", "components", "component"],
			],
			"operator":
			[
				["unary", "operator", "unary operator"],
				["binary", "operator", "binary operator"]
			]
		}

		for (const rule of rules[node.syntactic_type] || [])
		{
			const [parent_syntactic_type, parent_field, library_category] = rule;

			if (node.parent.syntactic_type === parent_syntactic_type)
			{
				const field = node.parent[parent_field];

				if (Array.isArray(field) && field.includes(node) || field === node)
				{
					const standard = library[library_category][node.text];

					const scope = standard ? "standard" : "user";

					const type = standard || node.semantic_type || node.parent.semantic_type;

					return `${scope} ${library_category} ${this.format_type(type)}`;
				}
			}
		}

		return "";
	}

	format_type (type)
	{
		if (type === undefined) return "";

		if (Array.isArray(type))
		{
			const return_type = type[0];
			const parameter_types = type.slice(1, type.length);

			return ` : (${parameter_types.join(`, `)}) -> ${return_type}`;
		}
		else if (type.return_type)
		{
			return ` : (${type.parameters.map(x => x.type).join(`, `)}) -> ${type.return_type}`;
		}
		else
		{
			return ` : ${type}`;
		}
	}

	semantic_tokens ()
	{
		const result = [];

		let last_line = 0;
		let last_character = 0;

		for (let line = 0; line < this.leaves.length; line++)
		{
			for (let leaf = 0; leaf < this.leaves[line].length; leaf++)
			{
				const current = this.leaves[line][leaf];

				if (leaf === 0)
				{
					result.push(current.position.start.line - last_line);
					result.push(current.position.start.character);
				}
				else
				{
					result.push(0);
					result.push(current.position.start.character - last_character);
				}

				result.push(current.position.end.character - current.position.start.character);

				const { type, modifiers } = this.semantic_token(current);

				result.push(type);
				result.push(modifiers);

				last_character = current.position.start.character;
				last_line = line;
			}
		}

		return result;
	}

	semantic_token (leaf)
	{
		let type = 5;
		let modifiers = 0;

		switch (leaf.	syntactic_type)
		{
			case "keyword":
			{
				type = leaf.text.match(/\w+/) ? 3 : 5;
			}
			break; case "operator":
			{
				type = 4;
				modifiers = 1;
			}
			break; case "identifier":
			{
				if (leaf.parent.syntactic_type === "system" && leaf.parent.name === leaf)
				{
					type = 0;
				}
				else if (leaf.parent.syntactic_type === "functional" && leaf.parent.name === leaf)
				{
					type = 0;
					modifiers = 1;
				}
				else if (leaf.parent.syntactic_type === "component" && leaf.parent.components.includes(leaf))
				{
					type = 2;
					if (library[categories.component][leaf.text])
					{
						modifiers = 1;
					}
				}
				else
				{
					type = 1;
					const standard = library[categories.value][leaf.text];
					if (standard && standard.constant)
					{
						modifiers = 3
					}
					else if (standard)
					{
						modifiers = 1;
					}
				}
			}
		}
		return { type, modifiers };
	}

	signatures (position)
	{
		const node = (this.leaves[position.line] || []).find(x => x.position.start.character <= position.character - 1 && x.position.end.character >= position.character);

		if (node === undefined)
		{
			return { signatures: [] };
		}
		if (node.parent.syntactic_type !== "functional") return null;

		const map_name = node.parent.name.text;

		const standard = library[categories.map][map_name];

		if (standard === undefined) return null;

		const formal_parameters = [];
		for (const parameter of standard.parameters)
		{
			formal_parameters.push({label: `${parameter.name} : ${parameter.type}`});
		}

		const signatures = [{label: `${map_name}(${formal_parameters.map(x => x.label).join(", ")})`, documentation: standard.documentation, parameters: formal_parameters}];

		return { signatures: signatures, activeSignature: 0, activeParameter: 0 }
	}
}

module.exports = { Insights }
