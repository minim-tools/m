import { createConnection, BrowserMessageReader, BrowserMessageWriter } from 'vscode-languageserver/browser';
const { TextDocumentSyncKind } = require("vscode-languageserver/node");
const { Generator } = require("../generation/generator");
const { Insights } = require("../insights/insights");
const { validate } = require("../../language/validator");
const { generate } = require("../../language/generator");
const { dirname } = require("path");

const connection = createConnection(new BrowserMessageReader(self), new BrowserMessageWriter(self));

let insights;
let workspace;

connection.listen();

connection.onInitialize ( async client =>
{
	workspace = client.workspaceFolders[0].uri;

	insights = new Insights();

	return { capabilities:
	{
		textDocumentSync: TextDocumentSyncKind.Full,
		hoverProvider: true,
		signatureHelpProvider:
		{
			triggerCharacters: ["(", ","]
		},
		semanticTokensProvider:
		{
			legend: { tokenTypes: insights.semantic_token_types, tokenModifiers: insights.semantic_token_modifiers },
			full: true
		}
	}};
});

connection.onDidOpenTextDocument( ({textDocument: {uri, text}}) =>
{
	run(uri, text);
});

connection.onDidChangeTextDocument( ({ contentChanges: [{text}], textDocument: {uri} }) =>
{
	run(uri, text);
});

connection.onHover(({position, textDocument: {uri}}) =>
{
	return { contents: insights.hover(position) };
});

connection.onRequest("textDocument/semanticTokens/full", ({textDocument: {uri}}) =>
{
	return { data: insights.semantic_tokens() };
});

connection.onRequest("textDocument/signatureHelp", ({position, textDocument: {uri}}) =>
{
	return insights.signatures(position);
});

async function run (uri, text)
{
	try
	{
		const { tree, leaves } = validate(text);

		connection.sendDiagnostics({uri, diagnostics: []});

		insights.leaves = leaves;

		const	generator = new Generator(dirname(uri));

		generate(tree, generator, text);

		await connection.workspace.applyEdit({documentChanges: generator.document_changes.filter(x=>x.kind)});
		await connection.workspace.applyEdit({documentChanges: generator.document_changes.filter(x=>!x.kind)});
	}
	catch (diagnostics)
	{
		if (Array.isArray(diagnostics))
		{
			connection.sendDiagnostics({uri, diagnostics});
		}
		else
		{
			connection.sendDiagnostics({uri, diagnostics: [{severity: 1, message: diagnostics+"\n"+diagnostics.stack, source: "m", range: {start: {line: 0, column: 0}, end: {line: 0, column: 0}}}]});
		}
	}
}

