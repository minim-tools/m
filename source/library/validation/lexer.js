exports.Lexer = class
{
	constructor (text, skip_regex = /[\r\n\t ]/)
	{
		this.text = text;
		this.index = 0;
		this.line = 0;
		this.character = 0;
		this.peek = this.text[this.index];
		this.errors = [];
		this.nodes = [{syntactic_type: "global", position: {}}];
		this.skip_regex = skip_regex;
		this.leaves = [];
		this.ghost_rules = [];
	}

	token (regex, name = "keyword")
	{
		const temp = {[name]: (node) =>
		{
			regex.lastIndex = this.index;
			const matches = regex.exec(this.text);
			if (matches)
			{
				node.text = matches[0];
				this.advance(node.text.length);
				node.position.end = { line: this.line, character: this.character, index: this.index };
				for (let i = this.leaves.length; i < this.line; i++)
				{
					this.leaves[i] = [];
				}
				if (this.leaves[this.line] === undefined)
				{
					this.leaves[this.line] = [node];
				}
				else
				{
					this.leaves[this.line].push(node);
				}
				return node;
			}
			else
			{
				if (name === "keyword")
				{
					this.error(regex.toString().replace(/^\//, "").replace(/\/y$/,"").replace("\\(", "(").replace("\\.", ".").replace("\\-", "-").replace("\\)", ")"));
				}
				else
				{
					this.error(name);
				}
			}
		}};

		temp[name].regex = regex;

		return temp[name];
	}

	eat (rule)
	{
		this.skip();

		const node = { parent: this.nodes[this.nodes.length-1], syntactic_type: rule.name, position: {start: {line: this.line, character: this.character, index: this.index}}};
		if ( ! this.ghost_rules.includes(rule) )
		{
			this.nodes.push(node);
		}

		const read = rule(node);

		if ( ! this.ghost_rules.includes(rule) )
		{
			this.nodes.pop();
		}

		if (read)
		{
			node.parent.position.end = node.position.end;
			if (this.ghost_rules.includes(rule))
			{
				return read;
			}
			else
			{
				return node;
			}
		}

		this.revert(node.position.start);
	}

	read (rule)
	{
		const node = this.eat(rule);

		if (node)
		{
			return node;
		}

		// let error = "Expected "+rule.name;

		// if (rule.regex)
		// {
		// 	error += " : "+rule.regex.toString().replace(/^\//, "").replace(/\/y$/,"").replace("\\(", "(").replace("\.", ".").replace("\\-", "-").replace("\\)", ")");
		// }

		// this.error(error);
	}

	assign (field, rule)
	{
		const node = this.eat(rule);

		if (node)
		{
			node.parent[field] = node;
			return node;
		}

		// const parent = this.nodes[this.nodes.length-1];
		// this.error("Expected "+field+" of "+parent.syntactic_type+" : "+rule.name);
	}

	push (field, rule)
	{
		const node = this.eat(rule);

		if (node)
		{
			node.parent[field] = [...(node.parent[field] || []), node];
			return node;
		}

		// const parent = this.nodes[this.nodes.length-1];
		// this.error("Expected "+field+" of "+parent.syntactic_type+" : "+rule.name);
	}

	optional (f)
	{
		const starting_position = { index: this.index, line: this.line, character: this.character };

		const read = f();

		if (!read)
		{
			this.revert(starting_position);
		}

		return true;
	}

	loop (f)
	{
		while (true)
		{
			const position = {index: this.index, line: this.line, character: this.character};
			const read = f();
			if (!read)
			{
				this.revert(position);
				break;
			}
		}

		return true;
	}

	error (message)
	{
		this.errors.push({ message: message, severity: 1, range: { start: {line: this.line, character: this.character}, end: {line: this.line, character: this.character + 1}}, index: this.index, depth: this.nodes.length });
	}

	skip ()
	{
		while (this.peek && this.skip_regex.exec(this.peek))
		{
			if (this.peek === "\n")
			{
				this.line++;
				this.character = -1;
			}
			this.advance();
		}
	}

	advance (amount = 1)
	{
		this.index += amount;
		this.character += amount;
		this.peek = this.text[this.index];
	}

	revert (position)
	{
		this.index = position.index;
		this.line = position.line;
		this.character = position.character;
		this.peek = this.text[this.index];

		for (let line = position.line; line < this.leaves.length; line++)
		{
			if (line === position.line)
			{
				this.leaves[line] = this.leaves[line].filter(x => x.position.start.character < position.character);
			}
			else
			{
				this.leaves[line] = [];
			}
		}
	}

	report ()
	{
		this.skip();
		if (this.index === this.text.length) return;

		let max_index_errors = [];
		let max_index = -1;

		for (const error of this.errors)
		{
			if (error.index > max_index)
			{
				max_index = error.index;
				max_index_errors = [error];
			}
			else if (error.index === max_index)
			{
				max_index_errors.push(error);
			}
		}

		const ordered_errors = [...new Set(max_index_errors.map(x => x.message))];

		let message = `Expected ${ordered_errors.slice(0, ordered_errors.length - 1).join(", ")}`;
		if (ordered_errors.length > 1)
		{
			message += ` or ${ordered_errors[ordered_errors.length - 1]}`;
		}
		max_index_errors[0].message = message;

		throw [max_index_errors[0]];
	}
}
