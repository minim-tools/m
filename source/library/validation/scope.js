const { library } = require("../../language/standard_library");

exports.Scope = class
{
	constructor ()
	{
		this.stack = [library, this.new_scope()];
	}

	new_scope ()
	{
		const symbols = {};
		for (const category in library)
		{
			symbols[category] = {};
		}
		return symbols;
	}

	push ()
	{
		this.stack.push(this.new_scope());
		return true;
	}
	pop ()
	{
		this.stack.pop();
		return true;
	}

	exists (category, name)
	{
		const stack = this.stack.find(x => x[category][name]);
		return stack && stack[category][name];
	}

	define (category, symbol)
	{
		const current = this.exists(category, symbol.text);

		if (current === undefined)
		{
			this.stack[this.stack.length - 1][category][symbol.text] = symbol;
		}
		else
		{
			throw [{severity: 1, source: "m", message: `${category} ${symbol.text} already defined`, range: symbol.position}];
		}

		return true;
	}

	access (category, node, accessor)
	{
		const current = this.exists(category, node.text);

		if (current === undefined)
		{
			throw [{severity: 1, source: "m", message: `${category} ${node.text} undefined`, range: node.position}];
		}
		else
		{
			accessor?.(current);
		}

		return true;
	}

	accessOrDefine(category, node, accessor)
	{
		const current = this.exists(category, node.text);

		if (current === undefined)
		{
			this.stack[this.stack.length - 1][category][node.text] = node;
		}
		else
		{
			accessor?.(current);
		}

		return true;
	}

	not(category, field, symbol)
	{
		const notCurrent = this.exists(category, symbol.text);
		if (notCurrent !== undefined && notCurrent[field])
		{
			throw [{severity: 1, source: "m", message: `${symbol.text} is a ${field} ${category}`, range: symbol.position}];
		}

		return true;
	}

	accessOrDefineGlobally(category, node, accessor)
	{
		const current = this.exists(category, node.text);

		if (current === undefined)
		{
			for (const scope of this.stack)
			{
				if (scope !== this.stack[0])
				{
					scope[category][node.text] = node;
				}
			}
		}
		else
		{
			accessor?.(current);
		}

		return true;
	}

	report ()
	{

	}
}
