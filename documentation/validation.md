# Validation

All valid source files are determined by the following grammar in cubic format:

~~~
file: systems+=system*
system: define system name=id { push statements+=statement+ pop }
statement: selection | iteration | assignment
selection: if condition=expression : proposition { push statements+=statement* pop }
iteration: if define variable iterator=id : entity (@ collection : entity_list) ? { push statements+=statement* pop }
assignment: address=(component | redefine not constant value id) : 0 = expression=expression : 0

expression: priority | transformation | data
priority : 0 : ( expression=expression : 0 )
transformation: unary | binary | functional
unary : 0[0]: access unary_operator operator=operator expression=expression : 0[1]
binary : 1[0]: left=expression : 1[1] access binary_operator operator=operator right=expression : 1[2]
functional : 0[0]: access function name=id ( (arguments+=expression : 0[n] (, arguments+=expression : 0[n])*)? )
data: component | access value id
component: access variable root=id : entity (. global redefine component entity_components+=id : entity)* . global redefine component component=id

id: /[Lowercase_]+/
operator: /[+-*/%<>=!|&@^#]+/
~~~

Literally every symbol here has a test, since it means a lot.
file: - the root node has type file
systems: - file has a systems field
+=: - systems field is an array
system: an array of systems
*: can hold any number of them, at it does hold them

There are 50 rules/tests in total: 20 syntax errors, 15 scoping errors and 15 type validation errors.

## Grammar

Systems are named list of statements.

Statements are iterations of selected assignments.

Expressions are prioritized transformations of data.

Transformations have unary, binary or functional form.

Data is stored as stack variables or heap ecs matrix cells.

### Scope: Undefined and redefined errors

Systems define a system name

Iterations define a value

{ and } define inner scopes

called functions must be in the standard library

sub_entities must be in the standard library

components are defined in global scope

address entities can be defined if assignment address, only accessed otherwise

### Types: Incompatible and undecidable errors

Iterations type entity

Selections type proposition

Assignment link address and expression

Priority is replaced by its expression

Binary types by standard operator

Unary types by standard operator

Call types by standard function

Value accesses are linked to definition

Component accesses are linked to definition

Subcomponent accesses are linked to definition
