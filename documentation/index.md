# M

Data oriented language for game developers.

## Story

## Grammar for languages

* | space are the control flow of the grammar: Translate to while, if and sequence.
: \n are the procedural structures of the grammar: Translate to start a function, end a function.
words are function calls

Any error inside the while breaks the loop. Loops are whiles.
Any error inside the if breaks the selection. Selections are ors.

Since you are never coming back from scoping or typing errors they can directly throw
Some syntax errors are recoverable because of selection and iteration paths.
The errors must be stored until recovered or until time to report when no more alternatives exist.

## Law of leaky abstractions

As every other abstraction, M is leaky. Meaning that it doesnt nor it tries to hide ALL the complexity related to building games.

Then why should you choose to use it over doing everything by yourself? Thats the real question to ask when using any tool really.

M simplifies the job of the programmer dramatically in the domain of its abstraction. It forces good practices to give game designers freedom, and the code it generates is comparably performant to a skilled programmer.

The greatest point is that, when the abstraction fails - a function is missing, or a complex logic is not idiomatic - it is easy to stop using M and do it with the appropriate tool. You can write C# code in unity and it integrates with M code without hassle. Or you can write an extension for M and pr.

The goal f M is to increase the domain of M so that more programmers decide to us it when it is applicable.

Thats the job of mathematicians. To find good abstractions. I dont claim to have found the greatest solution, but I have certainly iterated on it a lot. Like a lot.

## Building a pyramid metaphor

You can build a pyramid vertically or diagonally. Diagonally means that there is a time where the user does not see any new features,
but the pyramid can be built bigger thanks to it.

## Language particularities

- No number literals: Designers take care of balancing
- No direct references: No null references
- No type annotations: Still rich type errors

## Motivation

<h1 style="text-align: center">M</h1>
<h2 style="text-align: center">Jokoak garatzeko hizkuntza minimalista</h2>

<br>
<div style="font-size: 16px">
Hizkuntza formalek gure ideiak zehazki adierazten laguntzen digute. Partituren bitartez musika gida dezakegu. Semaforoekin berriz, trafikoaren fluxua.
Programazio hizkuntzek ordenagailuekin komunikazioa ahalbidetzen dute. Aginduak bitarrera itzultzeko moduan emanez gero, kexarik gabe beteko ditu. Hizkuntza hauetan oinarritu da informatikaren erreboluzioa. Saio honetan, berri bat erakutsi nahi dizuet: M.

<br>

Turing osoa da, baina ez da egokiena sare neuronalak entrenatzeko. Hasieratik, bideo jokoen garapena errazten izan da espezializatua. Jokoak sortzea entretenigarriaz gain komplexua baita.
Rol desberdinetako pertsonak bildu eta beren lana joko motoreetan batuz garatu ohi dira. M, zerotik hasi beharrean, motore ezagunenekin integratzen da eta profesionalentzako gehigarri interesgarria eskaintzen ditu. Programatzaile, diseinatzaile eta artisten lanak banandu eta paraleloan lan egiten laguntzen du. Tresna egokiekin garapen azkar *eta* mantengarria eskainiz.

Itxuraz familiarra egingo zaie C-ren familiako hizkuntzarik dakitenei. Ikasteko errazagoa, minimalista baita. Sintaxiaren kurtinen atzean badu ordea, berezitasunik. Datuetan oinarritatuko paradigma jarraitzen du. Ez du zenbakirik onartzen. Anotaziorik gabeko tipo sistema sendoa du. Eta laster ezinezkoa izango da memoria nulorik erreferentziatzea.

Ezaugarri hauen xehetasunak, baita hizkuntzarekin sortutako jokoen adibideak ere, hitzaldian azalduko ditugu.

</div>
