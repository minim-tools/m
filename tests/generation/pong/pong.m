control
{
	foreach paddle
	{
		paddle.velocity = paddle.direction *** input_number(paddle.axis, zero)
	}
}
serve
{
	foreach ball
	{
		if ball.ready
		{
			x = random(ball.m, ball.m)
			angle = random(ball.min_angle, ball.max_angle)
			ball.velocity = xyz(cos(angle), sin(angle), ball.z) *** ball.serve_length
			ball.ready = false
		}
	}
}
score
{
	foreach goal
	{
		foreach ball @ goal.collisions
		{
			ball.position = ball.reset_position
			ball.ready = true

			foreach board @ goal.boards
			{
				board.text = board.text + ball.worth

				if board.text > board.max_score
				{
					foreach global_board
					{
						global_board.text = global_board.reset_score
					}
				}
			}
		}
	}
}
