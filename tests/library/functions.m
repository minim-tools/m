system
{
	foreach a
	{
		a.proposition = input_triggered(a.input)
		a.number = input_number(a.input)

		a.number = abs(a.number)
		a.number = integer_part(a.number)
		a.number = sin(a.number)
		a.number = cos(a.number)
		a.number = tan(a.number)
		a.number = asin(a.number)
		a.number = acos(a.number)
		a.number = atan(a.number)
		a.number = exp(a.number)
		a.number = log(a.number)
		a.number = sqrt(a.number)
		a.number = max(a.number, a.number)
		a.number = min(a.number, a.number)
		a.number = pow(a.number, a.number)
		a.number = random(a.number, a.number)
		a.vector = cross(a.vector, a.vector)
		a.number = dot(a.vector, a.vector)
		a.number = length(a.vector)
		a.number = distance(a.vector, a.vector)
		a.quaternion = spherical_interpolation(a.quaternion, a.quaternion, a.number)

		a.vector = xyz(a.number, a.number, a.number)
		a.number = x(a.vector)
		a.number = y(a.vector)
		a.number = z(a.vector)

		a.quaternion = quaternion(a.vector)
		a.vector = vector(a.quaternion)
		a.vector = viewport(a.vector, a.entity)
		a.vector = world(a.vector, a.entity)
		a.text = text(a.number)
		a.number = number(a.text)
		a.number = degrees(a.number)
		a.number = radians(a.number)

		a.color = rgba(a.number, a.number, a.number, a.number)
		a.number = red(a.color)
		a.number = green(a.color)
		a.number = blue(a.color)
		a.number = alpha(a.color)

		a.entity = create(a)
		a.number = evaluate(a.curve, a.number)

		a.entity_list = ray(a.vector, a.vector, a.number)
		a.entity_list = box(a.vector, a.vector, a.quaternion)
		a.entity_list = sphere(a.vector, a.number)
	}
}
